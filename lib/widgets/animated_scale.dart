import 'package:flutter/material.dart';

class AnimatedScale extends ImplicitlyAnimatedWidget {
  const AnimatedScale({
    Key key,
    this.child,
    @required this.scale,
    Curve curve = Curves.ease,
    @required Duration duration,
    VoidCallback onEnd,
    this.alignment = Alignment.center,
  })  : assert(scale != null && scale >= 0.0),
        super(key: key, curve: curve, duration: duration, onEnd: onEnd);

  final double scale;

  final Widget child;

  final Alignment alignment;

  @override
  ImplicitlyAnimatedWidgetState<ImplicitlyAnimatedWidget> createState() =>
      _AnimatedScaleState();
}

class _AnimatedScaleState extends ImplicitlyAnimatedWidgetState<AnimatedScale> {
  Tween<double> _scale;
  Animation<double> _scaleAnimation;

  @override
  void forEachTween(TweenVisitor<dynamic> visitor) {
    _scale = visitor(
        _scale, widget.scale, (dynamic value) => Tween<double>(begin: value));
  }

  @override
  void didUpdateTweens() {
    _scaleAnimation = animation.drive(_scale);
  }

  @override
  Widget build(BuildContext context) {
    return ScaleTransition(
      scale: _scaleAnimation,
      child: widget.child,
      alignment: widget.alignment,
    );
  }
}
