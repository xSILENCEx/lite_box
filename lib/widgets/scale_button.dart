import 'package:flutter/material.dart';
import 'package:litetoolbox/helpers/app_helper.dart';

import 'animated_scale.dart';

class ScaleButton extends StatefulWidget {
  const ScaleButton({
    Key key,
    @required this.onTap,
    @required this.child,
    this.padding = EdgeInsets.zero,
    this.margin = EdgeInsets.zero,
    this.width = 80,
    this.height = 40,
    this.color = AppStyle.theme5,
    this.shadowColor = Colors.black,
  }) : super(key: key);

  ///宽
  final double width;

  ///高
  final double height;

  ///颜色
  final Color color;

  ///遮罩颜色
  final Color shadowColor;

  ///点击事件
  final Function onTap;

  ///子控件
  final Widget child;

  ///内边距
  final EdgeInsets padding;

  ///外边距
  final EdgeInsets margin;

  @override
  _ScaleButtonState createState() => _ScaleButtonState();
}

class _ScaleButtonState extends State<ScaleButton> {
  ///缩放
  double _scale = 1;

  ///遮罩透明度
  double _opacity = 0;

  ///按下
  _onTapDown(_) {
    setState(() {
      _scale = 0.9;
      _opacity = 0.4;
    });
  }

  ///抬起
  _onTapUp(_) {
    if (widget.onTap != null) widget.onTap();
    setState(() {
      _scale = 1;
      _opacity = 0;
    });
  }

  ///取消点击
  _onTapCancel() {
    setState(() {
      _scale = 1;
      _opacity = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedScale(
      scale: _scale,
      curve: Curves.easeInOut,
      duration: const Duration(milliseconds: 200),
      child: Padding(
        padding: widget.margin,
        child: GestureDetector(
          onTapDown: _onTapDown,
          onTapUp: _onTapUp,
          onTapCancel: _onTapCancel,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: Container(
              width: widget.width,
              height: widget.height,
              color: widget.color,
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  AnimatedOpacity(
                    opacity: _opacity,
                    curve: Curves.easeInOut,
                    duration: const Duration(milliseconds: 200),
                    child: Container(
                      color: widget.shadowColor,
                    ),
                  ),
                  Padding(
                    padding: widget.padding,
                    child: widget.child,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
