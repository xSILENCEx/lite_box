import 'package:flutter/material.dart';
import 'package:litetoolbox/widgets/scale_button.dart';

class CustomDialog {
  ///弹出提示对话框
  static Future<void> showTipsDialog(
    BuildContext context, {
    String title = '提示',
    String content = '内容',
    String buttonContentLeft = '同意',
    String buttonContentRight = '拒绝',

    ///确认
    Function onCheck,

    ///取消
    Function onCancel,
  }) async {
    await showCustomDialog(
      context: context,
      builder: (_) {
        return Material(
          textStyle: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
          color: Colors.black,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                title,
                style: const TextStyle(fontSize: 18),
              ),
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  physics: const BouncingScrollPhysics(),
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: Opacity(
                        opacity: 0.8,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(content),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  ScaleButton(
                    onTap: onCheck,
                    color: Colors.grey.withOpacity(0.4),
                    child: Text(buttonContentLeft),
                  ),
                  ScaleButton(
                    onTap: onCancel,
                    color: Colors.grey.withOpacity(0.4),
                    child: Text(buttonContentRight),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  ///弹出选项对话框
  static Future<void> showSelectDialog() async {}

  ///弹出自定义内容对话框
  static Future<void> showContentDialog() async {}
}

///自定义动画对话框
Future<T> showCustomDialog<T>({
  @required BuildContext context,
  bool barrierDismissible = true,
  WidgetBuilder builder,
}) {
  final ThemeData theme = Theme.of(context, shadowThemeOnly: true);
  return showGeneralDialog(
    context: context,
    pageBuilder: (BuildContext buildContext, Animation<double> animation,
        Animation<double> secondaryAnimation) {
      final Widget pageChild = Builder(builder: builder);
      return SafeArea(
        child: Builder(builder: (BuildContext context) {
          return theme != null
              ? Theme(data: theme, child: pageChild)
              : pageChild;
        }),
      );
    },
    barrierDismissible: barrierDismissible,
    barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
    barrierColor: Colors.black87, // 自定义遮罩颜色
    transitionDuration: const Duration(milliseconds: 150),
    transitionBuilder: _buildMaterialDialogTransitions,
  );
}

Widget _buildMaterialDialogTransitions(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child) {
  // 使用缩放动画
  return ScaleTransition(
    scale: CurvedAnimation(
      parent: animation,
      curve: Curves.easeOut,
    ),
    child: child,
  );
}
