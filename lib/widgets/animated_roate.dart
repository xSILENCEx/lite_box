import 'package:flutter/material.dart';

class AnimatedRotate extends ImplicitlyAnimatedWidget {
  const AnimatedRotate({
    Key key,
    this.child,
    @required this.angle,
    Curve curve = Curves.ease,
    @required Duration duration,
    VoidCallback onEnd,
    this.alignment = Alignment.center,
  }) : super(key: key, curve: curve, duration: duration, onEnd: onEnd);

  final double angle;

  final Widget child;

  final Alignment alignment;

  @override
  ImplicitlyAnimatedWidgetState<ImplicitlyAnimatedWidget> createState() =>
      _AnimatedRoateState();
}

class _AnimatedRoateState
    extends ImplicitlyAnimatedWidgetState<AnimatedRotate> {
  Tween<double> _angle;
  Animation<double> _rotateAnimation;

  @override
  void forEachTween(TweenVisitor<dynamic> visitor) {
    _angle = visitor(
        _angle, widget.angle, (dynamic value) => Tween<double>(begin: value));
  }

  @override
  void didUpdateTweens() {
    _rotateAnimation = animation.drive(_angle);
  }

  @override
  Widget build(BuildContext context) {
    return RotationTransition(
      turns: _rotateAnimation,
      child: widget.child,
      alignment: widget.alignment,
    );
  }
}
