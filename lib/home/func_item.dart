//功能按钮
import 'package:flutter/material.dart';
import 'package:litetoolbox/widgets/animated_scale.dart';

import 'func_icon.dart';

class FuncItem extends StatefulWidget {
  final int index;

  const FuncItem({Key key, this.index}) : super(key: key);

  @override
  _FuncItemState createState() => _FuncItemState();
}

class _FuncItemState extends State<FuncItem> {
  ///显示功能按钮
  bool _showItem;

  ///中心图标控件
  List<Widget> _center;

  @override
  void initState() {
    super.initState();
    _showItem = false;

    _center = [
      const FuncIcon(index: 0),
      const FuncIcon(index: 1),
      const FuncIcon(index: 2),
      const FuncIcon(index: 3),
    ];

    _ready();
  }

  ///初始动画
  Future<void> _ready() async {
    await Future.delayed(Duration(milliseconds: 300 + widget.index * 100),
        () => setState(() => _showItem = true));
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedScale(
      scale: _showItem ? 1 : 0,
      duration: const Duration(milliseconds: 300),
      child: _center[widget.index],
    );
  }
}
