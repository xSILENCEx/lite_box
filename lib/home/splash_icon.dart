import 'package:flutter/material.dart';

///闪屏页图标
class SplashIcon extends StatelessWidget {
  const SplashIcon({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 107,
      height: 112.5,
      child: Image.asset('images/color_logo.png'),
    );
  }
}
