import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:litetoolbox/coin/coin.dart';
import 'package:litetoolbox/compass/compass.dart';
import 'package:litetoolbox/flash_light/flash_light.dart';
import 'package:litetoolbox/helpers/app_helper.dart';
import 'package:litetoolbox/noise/noise.dart';
import 'package:screen/screen.dart';

///中心图标
class FuncIcon extends StatefulWidget {
  final int index;
  const FuncIcon({Key key, this.index}) : super(key: key);

  @override
  _FuncIconState createState() => _FuncIconState();
}

class _FuncIconState extends State<FuncIcon> {
  double _backUpBrightness;

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 0), () async {
      _backUpBrightness = await Screen.brightness;
    });
  }

  ///点击跳转
  Future<void> _onTap(BuildContext context, Widget page, int index) async {
    await Navigator.of(context)
        .push(CupertinoPageRoute(builder: (_) => page))
        .then((v) async {
      if (index == 3) {
        await Screen.setBrightness(_backUpBrightness);
        await Screen.keepOn(false);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    ///颜色
    const List<Color> _colors = [
      AppStyle.theme1,
      AppStyle.theme2,
      AppStyle.theme6,
      AppStyle.theme8,
    ];

    ///图标
    const List<List<Widget>> _icons = [
      ///指南针
      const [
        const Positioned(
          left: 14,
          bottom: 14,
          child: Icon(
            CustomIcons.compass_bg,
            size: 30,
            color: Color(0x7fffffff),
          ),
        ),
        const Positioned(
          right: 13,
          top: 13,
          child: Icon(
            CustomIcons.compass_fo,
            size: 30,
            color: Colors.white,
          ),
        ),
      ],

      ///分贝
      const [
        const Positioned(
          top: 18,
          left: 14,
          child: Icon(
            CustomIcons.noise_bg,
            size: 33,
            color: Color(0x7fffffff),
          ),
        ),
        const Positioned(
          top: 8,
          left: 10,
          child: Icon(
            CustomIcons.noise_fo,
            size: 50,
            color: Colors.white,
          ),
        ),
      ],

      ///硬币
      const [
        const Positioned(
          top: 10,
          left: 10,
          child: Icon(
            CustomIcons.coin_bg,
            size: 46,
            color: Color(0x7fffffff),
          ),
        ),
        const Positioned(
          top: 10,
          left: 10,
          child: Icon(
            CustomIcons.coin_fo,
            size: 39,
            color: Colors.white,
          ),
        ),
      ],

      ///手电筒
      const [
        const Positioned(
          top: 12,
          child: Icon(
            CustomIcons.flash_light_bg,
            size: 34,
            color: Color(0x7fffffff),
          ),
        ),
        const Positioned(
          child: Icon(
            CustomIcons.flash_light_fo,
            size: 50,
            color: Colors.white,
          ),
        ),
      ],
    ];

    ///页面
    const List<Widget> _funcPages = [
      const Compass(),
      const Noise(),
      const Coin(),
      const FlashLight(),
    ];

    return InkWell(
      borderRadius: BorderRadius.circular(100),
      onTap: () async =>
          await _onTap(context, _funcPages[widget.index], widget.index),
      child: Hero(
        tag: 'item${widget.index}',
        child: Container(
          width: 70,
          height: 70,
          child: Stack(
            alignment: Alignment.center,
            children: _icons[widget.index],
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            color: _colors[widget.index],
          ),
        ),
      ),
    );
  }
}
