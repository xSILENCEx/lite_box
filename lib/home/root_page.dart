import 'package:flutter/material.dart';

import 'splash_page.dart';

///请求权限
///准备数据
///闪屏页
class Rootpage extends StatelessWidget {
  const Rootpage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '小工具',
      debugShowCheckedModeBanner: false,
      home: const SplashPage(),
    );
  }
}
