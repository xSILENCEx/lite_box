import 'package:flutter/material.dart';

import 'func_item.dart';

class HomeBody extends StatelessWidget {
  const HomeBody({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const FuncItem(index: 0),
                const SizedBox(width: 10),
                const FuncItem(index: 1),
              ],
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const FuncItem(index: 2),
                const SizedBox(width: 10),
                const FuncItem(index: 3),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
