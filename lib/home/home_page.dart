import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:litetoolbox/home/home_body.dart';

///主页
class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ///滑动距离
  Offset _dX = Offset(0, 0);

  ///滑动起始位置
  double _sX = 0;

  _onDragStart(DragStartDetails d) {
    print('start ${d.localPosition.dx}');
    _sX = d.localPosition.dx;
  }

  _onDragUpdate(DragUpdateDetails d) {
    // print('update ${d.localPosition.dx}');
    if (d.localPosition.dx - _sX <= 0) return;

    setState(() => _dX = Offset(d.localPosition.dx - _sX, 0));
  }

  _onDragEnd(DragEndDetails d) {
    final double _screenWidth = MediaQueryData.fromWindow(window).size.width;

    if (_dX.dx < _screenWidth / 2) {
      setState(() => _dX = Offset(0, 0));
    } else {
      setState(() => _dX = Offset(_screenWidth - _sX, 0));
      SystemNavigator.pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragStart: _onDragStart,
      onHorizontalDragUpdate: _onDragUpdate,
      onHorizontalDragEnd: _onDragEnd,
      child: Transform.translate(
        offset: _dX,
        child: const HomeBody(),
      ),
    );
  }
}
