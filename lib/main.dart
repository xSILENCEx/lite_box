import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'home/root_page.dart';

void main() {
  runApp(const Rootpage());

  if (Platform.isAndroid) {
    ///状态栏透明
    SystemUiOverlayStyle systemUiOverlayStyle =
        SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);

    ///隐藏状态栏
    SystemChrome.setEnabledSystemUIOverlays([]);
  }
}
