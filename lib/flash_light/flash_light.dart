import 'package:flutter/material.dart';
import 'package:litetoolbox/helpers/app_helper.dart';
import 'package:litetoolbox/widgets/scale_button.dart';
import 'package:screen/screen.dart';

///手电筒
class FlashLight extends StatefulWidget {
  const FlashLight({Key key}) : super(key: key);

  @override
  _FlashLightState createState() => _FlashLightState();
}

class _FlashLightState extends State<FlashLight> {
  ///是否点亮手电
  bool _lighting = true;

  ///圆角
  bool _radius = false;

  @override
  void initState() {
    super.initState();

    _ready();
  }

  ///初始化
  Future<void> _ready() async {
    Future.delayed(const Duration(milliseconds: 500),
        () => setState(() => _radius = true));

    await Screen.keepOn(true);
    await Screen.setBrightness(1);
  }

  ///点亮或关闭手电
  void _light() => setState(() => _lighting = !_lighting);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'item3',
      child: Material(
        clipBehavior: Clip.antiAlias,
        borderRadius: BorderRadius.circular(_radius ? 0 : 30),
        color: _lighting ? Colors.white : Colors.black,
        child: GestureDetector(
          onTap: _light,
          child: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  width: double.infinity,
                  height: double.infinity,
                  alignment: Alignment.center,
                  color: Colors.transparent,
                  child: Text(_lighting ? '点击关闭手电筒' : '点击打开手电筒',
                      style: const TextStyle(color: Colors.grey, fontSize: 10)),
                ),
              ),
              ScaleButton(
                width: double.infinity,
                onTap: () => Navigator.pop(context),
                color: Colors.transparent,
                shadowColor: AppStyle.theme5,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const Icon(Icons.close, size: 20, color: Colors.grey),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
