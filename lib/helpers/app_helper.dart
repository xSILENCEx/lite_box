import 'package:flutter/material.dart';

///自定义图标
class CustomIcons {
  CustomIcons._();

  ///指南针
  static const IconData compass_fo = IconData(0xe606, fontFamily: 'Custom');
  static const IconData compass_bg = IconData(0xe609, fontFamily: 'Custom');

  ///分贝仪
  static const IconData noise_fo = IconData(0xe605, fontFamily: 'Custom');
  static const IconData noise_bg = IconData(0xe607, fontFamily: 'Custom');

  ///硬币
  static const IconData coin_fo = IconData(0xe60a, fontFamily: 'Custom');
  static const IconData coin_bg = IconData(0xe608, fontFamily: 'Custom');

  ///手电筒
  static const IconData flash_light_fo = IconData(0xe60b, fontFamily: 'Custom');
  static const IconData flash_light_bg = IconData(0xe60c, fontFamily: 'Custom');

  ///指南针刻度
  static const IconData compass_in = IconData(0xe60e, fontFamily: 'Custom');
  static const IconData compass_out = IconData(0xe60d, fontFamily: 'Custom');
}

///主题
class AppStyle {
  static const Color theme1 = Color(0xff2ad181);
  static const Color theme2 = Color(0xffea6057);
  static const Color theme3 = Color(0xffffaf00);
  static const Color theme4 = Color(0xff38d7d5);
  static const Color theme5 = Color(0xff0091ff);
  static const Color theme6 = Color(0xff6e5cff);
  static const Color theme7 = Color(0xff86de31);
  static const Color theme8 = Color(0xffff7d08);
  static const Color theme9 = Color(0xfff9391f);
}
