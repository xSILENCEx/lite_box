import 'package:flutter/material.dart';
import 'package:litetoolbox/helpers/app_helper.dart';

class CompassInner extends StatelessWidget {
  const CompassInner({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        const SizedBox(
          width: 160,
          height: 160,
          child: Icon(
            CustomIcons.compass_in,
            size: 140,
            color: Colors.white,
          ),
        ),
        const Positioned(
          left: 70,
          top: 20,
          child: SizedBox(
            width: 20,
            height: 20,
            child: Center(child: Text('N')),
          ),
        ),
        const Positioned(
          left: 70,
          bottom: 20,
          child: SizedBox(
            width: 20,
            height: 20,
            child: Center(child: Text('S')),
          ),
        ),
        const Positioned(
          left: 20,
          top: 70,
          child: RotatedBox(
            quarterTurns: 3,
            child: SizedBox(
              width: 20,
              height: 20,
              child: Center(child: Text('W')),
            ),
          ),
        ),
        const Positioned(
          right: 20,
          top: 70,
          child: RotatedBox(
            quarterTurns: 1,
            child: SizedBox(
              width: 20,
              height: 20,
              child: Center(child: Text('E')),
            ),
          ),
        ),
      ],
    );
  }
}
