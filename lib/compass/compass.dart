import 'package:flutter/material.dart';
import 'package:flutter_compass/flutter_compass.dart';
import 'package:flutter_vibrate/flutter_vibrate.dart';
import 'package:litetoolbox/helpers/app_helper.dart';

import 'compass_inner.dart';

class Compass extends StatelessWidget {
  const Compass({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'item0',
      child: Material(
        color: Colors.black,
        textStyle: const TextStyle(color: Colors.grey, fontSize: 10),
        child: Center(
          child: StreamBuilder<double>(
            stream: FlutterCompass.events,
            builder: (context, snapshot) {
              if (snapshot.hasError) return Text('出现错误: ${snapshot.error}');

              ///加载
              if (snapshot.connectionState == ConnectionState.waiting)
                return Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    const RotationTransition(
                      turns: const AlwaysStoppedAnimation(0),
                      child: const CompassInner(),
                    ),

                    ///外圈指针
                    const SizedBox(
                      width: 180,
                      height: 180,
                      child: Icon(
                        CustomIcons.compass_out,
                        size: 156,
                        color: Colors.red,
                      ),
                    ),

                    Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Text(
                          '0°',
                          style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.red,
                          ),
                        ),
                        const Text('--'),
                      ],
                    ),
                  ],
                );

              ///获取角度
              final double _direction = snapshot.data ?? -1;

              ///震动反馈
              final int _feedback = _direction.floor();
              if (_feedback == 0 ||
                  _feedback == 90 ||
                  _feedback == 180 ||
                  _feedback == 270) {
                Vibrate.feedback(FeedbackType.impact);
              }

              ///方位
              const List<String> _position = [
                '正北',
                '东北',
                '正东',
                '东南',
                '正南',
                '西南',
                '正西',
                '西北',
                '设备不支持',
              ];

              ///方位索引
              int _positionIndex = 8;
              if (_direction <= 0) {
                _positionIndex = 8;
              } else if (_direction >= 355 || _direction < 5)
                _positionIndex = 0;
              else if (_direction >= 5 && _direction < 85)
                _positionIndex = 1;
              else if (_direction >= 85 && _direction < 95)
                _positionIndex = 2;
              else if (_direction >= 95 && _direction < 175)
                _positionIndex = 3;
              else if (_direction >= 175 && _direction < 185)
                _positionIndex = 4;
              else if (_direction >= 185 && _direction < 265)
                _positionIndex = 5;
              else if (_direction >= 265 && _direction < 275)
                _positionIndex = 6;
              else if (_direction >= 275 && _direction < 355)
                _positionIndex = 7;

              ///正常显示
              return Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  ///旋转部分
                  RotationTransition(
                    turns: AlwaysStoppedAnimation(1 - _direction / 360),
                    child: const CompassInner(),
                  ),

                  ///外圈指针
                  const SizedBox(
                    width: 180,
                    height: 180,
                    child: Icon(CustomIcons.compass_out,
                        size: 156, color: Colors.red),
                  ),

                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        _positionIndex == 8 ? 'NULL' : '${_direction.floor()}°',
                        style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.red),
                      ),
                      Text('${_position[_positionIndex]}'),
                    ],
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
